package com.company;

import java.util.Scanner;

public class Main {

    //Homework

    //For 1,2 task
    static boolean Max(int a,int b)
    {
        return a>b;
    }

    //For 1 task
    static int ChoosingMax(int a,int b, int max)
    {
        max=0;

        if(Max(a,b))
        {
            return max=a;
        }
        else
        {
            return max=b;
        }
    }


    //For 2 task
    static int ChoosingMaxInCount(int a, int b, int c, int d, int e, int f, int max)
    {
        max=0;
        if(Max(a,b))
        {
            return max=a;
        }
        else if(Max(b,c))
        {
            return max=b;
        }
        else if(Max(c,d))
        {
            return max=c;
        }
        else if(Max(d,e))
        {
            return max=d;
        }
        else if(Max(e,f))
        {
            return max=e;
        }
        else
        {
            return max=f;
        }
    }

    //For 3 task
    static boolean LuckyTicket(int ticket)
    {
        int a=ticket/100000;
        int b=(ticket/10)%10;
        int c=(ticket/100)%10;
        int d=(ticket/1000)%10;
        int e=(ticket/10000)%10;
        int f=ticket%10;

        int sum1=a+b+c;
        int sum2=d+e+f;

        return sum1==sum2;
    }
    //For 4,5 task
    static void PrintlnInt(int x)
    {
        System.out.println(x);
    }

    //For 4 task
    static void PlochadPryam(int a, int b)
    {
        PrintlnInt(a*b);
    }

    //For 5 task
    static int PerimPryam(int a, int b)
    {
        return 2*(a+b);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //1
        /*int first, second,max=0;

        System.out.println("Input the first number");
        first=scanner.nextInt();
        System.out.println("Input the second number");
        second=scanner.nextInt();

        System.out.println("max = "+ChoosingMax(first,second,max));*/

        //2
        /*int first, second,third,fourth,fifth,sixth,max=0;

        System.out.println("Input the first number");
        first=scanner.nextInt();
        System.out.println("Input the second number");
        second=scanner.nextInt();
        System.out.println("Input the third number");
        third=scanner.nextInt();
        System.out.println("Input the fourth number");
        fourth=scanner.nextInt();
        System.out.println("Input the fifth number");
        fifth=scanner.nextInt();
        System.out.println("Input the sixth number");
        sixth=scanner.nextInt();

        System.out.println("max = "+ChoosingMaxInCount(first,second,third,fourth,fifth,sixth,max));*/

        //3

        /*int ticket;

        System.out.println("Input ticket");
        ticket=scanner.nextInt();

        if (LuckyTicket(ticket))
        {
            System.out.println(LuckyTicket(ticket));
        }
        else
        {
            System.out.println(LuckyTicket(ticket));
        }*/

        //4

        /*int height, width;

        System.out.println("Input height");
        height=scanner.nextInt();
        System.out.println("Input width");
        width=scanner.nextInt();

        PlochadPryam(height,width);*/

        //5

        int height, width;

        System.out.println("Input height");
        height=scanner.nextInt();
        System.out.println("Input width");
        width=scanner.nextInt();

        PrintlnInt(PerimPryam(height,width));
    }
}
